/* 15 de abril ejercicio 3
Escribir un programa en C que mediante una funcion realice el calculo del 
factorial de un numero entero positivo que el usuario podr� ingresar por 
teclado. Terminar el programa cuando el usuario ingrese un 0.*/

#include <stdio.h>
#include <stdlib.h>

int factorial (int);

int main(int argc, char *argv[]) {
	int fact;
	int aux;
	
	do
	{
		system ("cls");
		printf ("Ingrese 0 para salir");
		printf ("\n\nCalcular el factorial de: ");
		scanf ("%d", &fact);
		printf ("\nEl factorial de %d es: ", fact);
		aux= factorial(fact);
		printf ("%d\n\n", aux);
		system ("pause");
		
	}while (fact != 0);

	return 0;
}

int factorial (int fact)
{
	int i;
	
	for (i=fact; i>1; i--)
	{
		fact= fact * (i-1);
	}
	return fact;
}
