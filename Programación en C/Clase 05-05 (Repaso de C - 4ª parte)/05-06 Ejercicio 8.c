/* 6 de mayo ejercicio 8
Crear una funci�n que permita obtener la parte izquierda de una cadena de 
caracteres definida por el n�mero de caracteres que la forman.*/

#include <stdio.h>

int main(int argc, char *argv[]) {
	int count= -1;
	int i=0;
	char s1[30];
	char s2[30]={0};
	
	printf ("Ingrese una frase: ");
	fgets (s1, 30, stdin);
	while (s1[i] != '\0')
	{
		count += 1;
		i++;
	}
	printf ("\nSe ingresaron %d caracteres.\n", count);
	for (i=0; i<count/2; i++)
	{
		s2[i]=s1[i];
	}
	printf("\nMitad izquierda de la cadena: %s", s2);
	return 0;
}
