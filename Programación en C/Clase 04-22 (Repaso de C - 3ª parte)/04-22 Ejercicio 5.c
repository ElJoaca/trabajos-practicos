/* 22 de abril ejercicio 5
�Ser� posible llamar a la funcion main de manera recursiva? Escriba un programa
que contenga una funcion main. Incluya una variable static cuenta, inicializada 
en 1. Postincremente e imprima el valor de cuenta cada vez que se invoque a 
main. Ejecute el programa. �Qu� sucede?*/

#include <stdio.h>

int main() {
	static int cuenta= 1;
	
	cuenta++;
	printf ("Cuenta: %d", cuenta);
	main ();

	return 0;
}

