/* 22 de abril ejercicio 2
Implementar una funcion en C para el calculo del factorial de un numero entero
mediante recursividad. Demostrar su funcionamiento en un programa completo.*/

#include <stdio.h>

int factorial (int);

int main(int argc, char *argv[]) {
	int num;
	
	printf ("Ingrese un numero: ");
	scanf ("%d", &num);
	printf ("\nEl factorial de %d es: %d", num, factorial(num));
	return 0;
}

int factorial (int num) 
{
	if (num == 1 || num == 0)
	{
		return 1;
	}
	else 
	{
		return num * factorial (num-1);
	}
}
