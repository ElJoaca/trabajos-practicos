/* 22 de abril ejercicio 3
Implementar una funcion en C que devuelva el elemento N de la serie de Fibonacci 
mediante recursión. Fibonacci(0)=1 Fibonacci(1)=1 
Fibonacci(n)=Fibonacci(n-1)+Fibonacci(n-2)*/

#include <stdio.h>

int fibonacci (int);

int main(int argc, char *argv[]) {
	int num;
	
	printf ("Ingrese un numero: ");
	scanf ("%d", &num);
	printf ("\nEl elemento %d de la serie de fibonacci es: %d", num, fibonacci(num));
	return 0;
}

int fibonacci (int num)
{
	if (num == 1 || num == 0)
	{
		return 1;
	}
	else 
	{
		return fibonacci (num-1) + fibonacci (num-2);
	}
}
